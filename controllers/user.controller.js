
const db = require('../config');
const ObjectID = require('mongodb').ObjectID;

exports.addRecord = async (req, res) => {
    try {
        let temp = req.body;
        temp["created"] = new Date();
        let addedRecord = await db.getMongoConnetion().collection('userData').insertOne(req.body)
        res.status(200).send({ "responseStatus": 1, data: addedRecord.result, message: "New record added successfully." });
    }
    catch (err) {
        res.status(500).send({ "responseStatus": 0, errors: err, message: "Something went wrong !!" });
    }
};

exports.getAllrecords = async (req, res) => {
    try {
        let allRecords = await db.getMongoConnetion().collection('userData').find().sort({ created: -1 }).toArray()
        res.status(200).send({ "responseStatus": 1, data: allRecords, message: "All user's data list." });
    }
    catch (err) {
        res.status(500).send({ "responseStatus": 0, errors: err, message: "Something went wrong !!" });
    }
}

exports.deleteRecords = async (req, res) => {
    try {
        let deletedRecords = await db.getMongoConnetion().collection('userData').deleteOne({ "_id": ObjectID(req.params.id) })
        res.status(200).send({ "responseStatus": 1, data: deletedRecords.result, message: "Record deleted successfully." });
    }
    catch (err) {
        res.status(500).send({ "responseStatus": 0, errors: err, message: "Something went wrong !!" });
    }
}

exports.updateRecords = async (req, res) => {
    try {
        let updatedRecords = await db.getMongoConnetion().collection('userData').updateOne({ "_id": ObjectID(req.body.id) }, {
            $set: {
                name: req.body.name,
                email: req.body.email,
                phone: req.body.phone,
                address: req.body.address,
                created: new Date()
            }
        })
        res.status(200).send({ "responseStatus": 1, data: updatedRecords.result, message: "Record updated successfully." });
    }
    catch (err) {
        res.status(500).send({ "responseStatus": 0, errors: err, message: "Something went wrong !!" });
    }
}
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const dbConnection = require("./config")
const cors = require("cors");
const userRoutes = require("./routes/user.route");

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false, parameterLimit: 1000000 }));
app.use(cors());

app.use("/kira19/v1/", userRoutes);

app.listen(3001, () => {
    console.log(`Server is running on 3001 successfully`);
    dbConnection.connection;
});
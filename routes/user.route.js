const express = require("express");
const route = express.Router();
const userController = require('../controllers/user.controller')

route.post('/addNewRecord', userController.addRecord)
route.get('/getAllRecords', userController.getAllrecords)
route.delete('/deleteRecord/:id', userController.deleteRecords)
route.post('/updateRecord', userController.updateRecords)
module.exports = route
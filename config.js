const client = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/kira19DB"
var mongoConnection;

module.exports.connection = client.connect(url, { useUnifiedTopology: true })
  .then(client => {
    console.log('Connected to Database')
    mongoConnection = client.db('kira19DB')
  })

  module.exports.getMongoConnetion = () => { 
    if(!mongoConnection) {
        throw new Error('Call InitiateMongoConnection first!');
    }
    return mongoConnection;
}